package de.bixilon.mcscanhack.main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class Main {
	public static String host = "localhost";
	public static int port_start = 1;
	public static int port_end = 65535;
	public static long start_time;
	public static int timeout = 200;

	static int result = 0;
	static int resultmc = 0;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a host [127.0.0.1]: ");
		host = scanner.nextLine();
		System.out.print("Enter a port range [1-65535]: ");
		String raw = scanner.nextLine();
		if (raw.length() != 0) {
			try {
				port_start = Integer.valueOf(raw.split("-", 2)[0]);
				port_end = Integer.valueOf(raw.split("-", 2)[1]);
				if (port_start <= 0 || port_start > 65535 || port_end <= 0 || port_end > 65535) {
					System.out.println("Invalid port!");
					System.exit(0);
					;
				}
			} catch (Exception e) {
				System.out.println("Invalid port!");
				System.exit(0);
			}

		}
		System.out.print("Enter timeout [200]: ");
		raw = scanner.nextLine();
		if (raw.length() != 0)
			timeout = Integer.valueOf(scanner.nextLine());
		scanner.close();
		start_time = System.currentTimeMillis();
		for (int i = port_start; i < port_end; i++) {
			Socket s = new Socket();
			try {
				s.setSoTimeout(timeout);
			} catch (SocketException e) {
				e.printStackTrace();
			}
			try {
				s.connect(new InetSocketAddress(host, i), timeout);
			} catch (IOException e) {
				// not listening
				continue;
			}
			System.out.print("Port " + i + ":		Open	");

			result++;
			DataOutputStream out;
			try {
				out = new DataOutputStream(s.getOutputStream());
				DataInputStream in = new DataInputStream(s.getInputStream());

				out.write(0xFE);

				StringBuffer str = new StringBuffer();
				int b = -1;

				while ((b = in.read()) != -1) {
					if (b != -1) {
						if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
							str.append((char) b);
						}
					}
				}

				s.close();
				String[] data = str.toString().split("§");
				System.out.println(Integer.parseInt(data[1]) + "/" + Integer.parseInt(data[2]));
				resultmc++;

			} catch (Exception e) {
				System.out.println("OFFLINE");
			}
		}
		System.out.println("Scan done. Found " + result + " open ports and " + resultmc + " minecraft servers.\nIt took " + formatMillis(System.currentTimeMillis() - start_time) + " to complete ");

	}

	static public String formatMillis(long val) {
		StringBuilder buf = new StringBuilder(20);
		String sgn = "";

		if (val < 0) {
			sgn = "-";
			val = Math.abs(val);
		}

		append(buf, sgn, 0, (val / 3600000));
		val %= 3600000;
		append(buf, ":", 2, (val / 60000));
		val %= 60000;
		append(buf, ":", 2, (val / 1000));
		val %= 1000;
		append(buf, ".", 3, (val));
		return buf.toString();
	}

	/**
	 * Append a right-aligned and zero-padded numeric value to a `StringBuilder`.
	 */
	static private void append(StringBuilder tgt, String pfx, int dgt, long val) {
		tgt.append(pfx);
		if (dgt > 1) {
			int pad = (dgt - 1);
			for (long xa = val; xa > 9 && pad > 0; xa /= 10) {
				pad--;
			}
			for (int xa = 0; xa < pad; xa++) {
				tgt.append('0');
			}
		}
		tgt.append(val);
	}

}
